import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import {
  faUserCircle,
  faPlane,
  faCreditCard,
  faLifeRing,
  faPhoneAlt,
  faSignOutAlt,
  faQuestionCircle,
  faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';

import Container from '../containers/Container';
import MenuHeader from '../containers/MenuHeader';
import Divider from '../containers/Divider';
import Tile from '../containers/MenuTile';
import ColumnWrapper from '../containers/ColumnWrapper';
import MenuFooter from '../containers/MenuFooter';
import UserProfile from '../containers/UserProfile';
import MenuContent from '../containers/MenuContent';

import './Menu.css';

function Menu() {
  const [open, setOpen] = useState(true);
  const [width, setWidth] = useState(window.innerWidth);

  const updateWidth = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', updateWidth);
    return () => {
      window.removeEventListener('resize', updateWidth);
    };
  });

  return (
    <CSSTransition
      in={open}
      timeout={300}
      onEnter={() => setOpen(true)}
      onExited={() => setOpen(false)}
      unmountOnExit
      classNames="menu"
    >
      <Container direction="column">
        <MenuHeader onCancel={() => setOpen(!open)} />
        <Divider />
        <MenuContent flex={1}>
          <ColumnWrapper screenWidth={width} flex={2}>
            <Tile>Home</Tile>
            <Tile>Flights</Tile>
            <Divider />
            <Tile mobileOrder={7} screenWidth={width} icon={faQuestionCircle}>About us</Tile>
            <Tile mobileOrder={8} screenWidth={width} icon={faInfoCircle}>FAQ</Tile>
            <Tile mobileOrder={4} screenWidth={width} icon={faLifeRing}>Support</Tile>
            <Tile mobileOrder={5} screenWidth={width} icon={faPhoneAlt}>Contact us</Tile>
          </ColumnWrapper>
          <ColumnWrapper screenWidth={width} flex={3}>
            <UserProfile screenWidth={width} />
            <Tile mobileOrder={1} screenWidth={width} icon={faUserCircle}>Profile</Tile>
            <Tile mobileOrder={2} screenWidth={width} icon={faPlane}>My Bookings</Tile>
            <Tile mobileOrder={3} screenWidth={width} icon={faCreditCard}>My Payments</Tile>
            <Tile mobileOrder={6} screenWidth={width} icon={faSignOutAlt}>Log Out</Tile>
            <Tile>Resume Application</Tile>
          </ColumnWrapper>
          <ColumnWrapper screenWidth={width} flex={2} />
        </MenuContent>
        <Divider />
        <MenuFooter>
          {"We're here to help"}
        </MenuFooter>
      </Container>
    </CSSTransition>
  );
}

export default Menu;
