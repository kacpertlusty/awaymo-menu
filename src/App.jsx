import React from 'react';
import { ThemeProvider } from 'styled-components';

import Menu from './components/Menu';

const theme = {
  backgroundColor: '#EE5F64',
  primary: 'white',
  secondary: '#F1888F',
};

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Menu />
      </ThemeProvider>
    </div>
  );
}

export default App;
