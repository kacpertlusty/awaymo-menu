import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import devices, { widths } from '../utils/devices';

const Avatar = styled.img`
  border-radius: 50%;
  padding: 15px;
  background-color: ${(props) => props.theme.primary};
  height: 30px;
  width: 30px;
  justify-content: center;
  
  @media ${devices.tablet} {
    justify-content: flex-start;
  }
`;

const GradientBorder = styled.div`
  display: flex;
  height: 65px;
  width: 65px;
  padding: 2.5px;
  background: linear-gradient(to right, red, purple);
  border-radius: 50%;
  justify-content: center;
  align-items: center;
  border: 5px solid ${(props) => props.theme.primary};
`;

const GradientBorderWrapper = (props) => {
  const { isMobile, children } = props;
  if (isMobile) {
    return <>{children}</>;
  }
  return (
    <GradientBorder>
      {children}
    </GradientBorder>
  );
};

GradientBorderWrapper.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
};

const ProfileInfo = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 10px;

  @media ${devices.tablet} {
    margin-left: 25px;
  };
`;

const Label = styled.div`
  color: ${(props) => props.theme.primary};
  text-align: center;
  font-size: 15px;
  @media ${devices.tablet} {
    text-align: left;
    font-size: 20px;
    font-weight: bold;
  }
`;

const NameLabel = styled(Label)`
  font-size: 20px;
`;

const ProfileRow = styled.div`
  order: -1;
  display: flex;
  padding-bottom: 10px;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  border-bottom: unset;

  @media ${devices.tablet} {
    flex-direction: row; 
    border-bottom: 1px solid ${(props) => props.theme.secondary};
    align-items: flex-start;
    justify-content: flex-start;
  }

`;


const UserProfile = ({ screenWidth, ...props }) => {
  const isMobile = screenWidth < widths.tablet;

  return (
    <ProfileRow {...props}>
      <GradientBorderWrapper isMobile={isMobile}>
        <Avatar src="avatar.png" screenWidth />
      </GradientBorderWrapper>
      <ProfileInfo>
        <NameLabel key="name">
Dominik
          {' '}
          {isMobile ? '' : 'Biel'}
        </NameLabel>
        {
          isMobile ? (
            <>
              <Label key="balanceDesc">Available Balance</Label>
              <Label key="balanceVal">£1,500.00</Label>
            </>
          )
            : <Label key="balance">£1,500.00 Available</Label>
        }
      </ProfileInfo>
    </ProfileRow>
  );
};

UserProfile.propTypes = {
  screenWidth: PropTypes.number.isRequired,
};

export default UserProfile;
