import styled from 'styled-components';

const Divider = styled.div`
  margin: 10px;
`;

export default Divider;
