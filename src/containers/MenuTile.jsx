import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Icon from './Icon';
import devices from '../utils/devices';

const Link = ({ children, ...props }) => (
  <a {...props}>
    {children}
  </a>
);

Link.propTypes = {
  children: PropTypes.string.isRequired,
};

const StyledLink = styled(Link)`
  font-size: 15px;
  color: ${(props) => props.theme.primary};
  margin-left: 10px;
  
  @media ${devices.phone} {
    font-size: 18px;
  }

  @media ${devices.tablet} {
    font-size: 30px;
    font-weight: bold;
  }
`;

const TileStyled = styled.div`
  margin-top: 15px;
  border-bottom: 1px solid ${(props) => props.theme.secondary};
  margin-right: 15px;
  margin-left: 15px;
  padding-bottom: 15px;
  order: ${(props) => props.mobileOrder || 0};
  display: ${(props) => (props.mobileOrder ? '' : 'None')};

  @media ${devices.tablet} {
    border-bottom: unset;
    margin-right: 0px;
    margin-left: 0px;
    padding-bottom: 0px;
    order: 0;
    display: inline;
  };
  
  &:nth-child(5) {
    border-bottom: unset;
  };
`;

const Tile = ({
  screenWidth, icon, children, ...props
}) => {
  if (screenWidth < 768) {
    const iconComponent = icon ? <Icon icon={icon} /> : <></>;
    return (
      <TileStyled {...props}>
        {iconComponent}
        <StyledLink>{children}</StyledLink>
      </TileStyled>
    );
  }
  return (
    <TileStyled {...props}>
      <StyledLink>{children}</StyledLink>
    </TileStyled>
  );
};

Tile.propTypes = {
  screenWidth: PropTypes.number,
  icon: PropTypes.shape({
    prefix: PropTypes.string,
    iconName: PropTypes.string,
    icon: PropTypes.arrayOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.array,
    ])),
  }),
  children: PropTypes.string.isRequired,
};

Tile.defaultProps = {
  screenWidth: 0,
  icon: null,
};

export default Tile;
