import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const Icon = styled(FontAwesomeIcon)`
  color: ${(props) => props.theme.primary};  
  min-width: 15px;
  height: 15px;
`;

export default Icon;
