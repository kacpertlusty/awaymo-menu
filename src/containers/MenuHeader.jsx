import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

const Bar = styled.div`
  min-height: 10vh;
  display: flex;
  margin-left: 10px;
  margin-right: 10px;
  border-bottom: solid 1px white;
  justify-content: center;
  @media (max-width: 640px) {
    border-bottom: solid 1px #F1888F;
  }
`;

const CloseIcon = (props) => (
  <FontAwesomeIcon icon={faTimes} {...props} />
);

const CloseButton = styled(CloseIcon)`
  color: white;
  position: absolute;
  right: 10px;
  top: 4vh;
  vertical-align: middle;
`;

const Logo = styled.img`
  vertical-align: middle;
`;

const MenuHeader = (props) => {
  const { onCancel } = props;
  return (
    <Bar>
      <Logo src="awaymoFullWhite.svg" />
      <CloseButton onClick={onCancel} />
    </Bar>
  );
};

MenuHeader.propTypes = {
  onCancel: PropTypes.func.isRequired,
};

export default MenuHeader;
