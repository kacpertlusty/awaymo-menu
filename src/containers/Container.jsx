import styled from 'styled-components';

const Container = styled.div`
  min-width: 100%;
  min-height: 100vh;
  z-index: 1;
  background: ${(props) => props.theme.backgroundColor};
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: flex-start;
`;

export default Container;
