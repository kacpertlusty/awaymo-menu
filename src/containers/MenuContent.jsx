import styled from 'styled-components';

const MenuContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  align-content: stretch;
  width: ${(props) => props.width};
  flex: ${(props) => props.flex};
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export default MenuContent;
