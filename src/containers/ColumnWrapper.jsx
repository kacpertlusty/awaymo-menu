import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ColumnStyle = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  flex: ${(props) => props.flex};
`;

function ColumnWrapper({ children, screenWidth, ...props }) {
  if (screenWidth > 768) {
    return <ColumnStyle {...props}>{children}</ColumnStyle>;
  }
  return <>{children}</>;
}

ColumnWrapper.propTypes = {
  screenWidth: PropTypes.number.isRequired,
  children: PropTypes.arrayOf(PropTypes.node),
};

ColumnWrapper.defaultProps = {
  children: [],
};

export default ColumnWrapper;
