import React from 'react';
import styled from 'styled-components';
import devices from '../utils/devices';

const StyledText = styled.p`
  line-height: 10px;
`;

const UnstyledFooter = ({ ...props }) => (
  <div {...props}>
    <StyledText>
      {"We're here to help"}
    </StyledText>
    <StyledText>+44 (0) 20 8050 3459</StyledText>
    <StyledText>support@awaymo.com</StyledText>
  </div>
);

const MenuFooter = styled(UnstyledFooter)`
  color: ${(props) => props.theme.primary};
  text-align: center;
  margin-left: 10px;
  margin-right: 10px;
  border-top: 1px solid ${(props) => props.theme.secondary};
  min-height: 10vh;

  @media ${devices.tablet} {
    min-height: 20vh;
  };
`;

export default MenuFooter;
