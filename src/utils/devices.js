export const widths = {
  phone: 640,
  tablet: 768,
  laptop: 1024,
};

const size = {
  phone: `${widths.phone}px`,
  tablet: `${widths.tablet}px`,
  laptop: `${widths.laptop}px`,
};

const devices = {
  phone: `(min-width: ${size.phone})`,
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
};

export default devices;
